#Réalisé par Adrien Giot et Guillaume Jadin
def gauss_jordan(l):
    nb_lignes = len(l)
    nb_col = len(l[0])
    ligne_pivot = -1

    if nb_col > nb_lignes:
        nb = nb_lignes
    else:
        nb = nb_col

    for i in range(nb):  # nombre de colonnes donné par la longueur de ligne
        colonne = [l[j][i] for j in range(nb_lignes)]  # j prend l'index des lignes
        pivot = colonne[ligne_pivot + 1]

        for j in range(ligne_pivot + 2, nb_lignes):
            if colonne[j] > pivot:
                pivot = colonne[j]

        ligne_max = colonne[ligne_pivot + 1:].index(pivot) + (ligne_pivot + 1)

        if pivot != 0:
            ligne_pivot += 1
            for j in range(nb_col):  # chaque élément de la ligne
                l[ligne_max][j] /= pivot

            if ligne_pivot != ligne_max:  # Echange les emplacements de deux lignes
                l[ligne_pivot], l[ligne_max] = l[ligne_max], l[ligne_pivot]

            for k in range(nb_lignes):
                if k != ligne_pivot:
                    value = l[k][i]

                    for chaque_col in range(nb_col):
                        l[k][chaque_col] -= value * l[ligne_pivot][chaque_col]

    for i in range(nb_lignes):  # parcoure la matrice pour changer les -0.0 en 0
        for j in range(nb_col):
            if l[i][j] == -0.0:
                l[i][j] = 0.0

    return l


d = {"A": [[7.0,2.0],[1.0,3.0]],
     "B": [[3.0,4.0,2.0],[5.0,3.0,5.0]],
     "C": [[4.0,1.0,3.0],[6.0,1.0,3.0],[4.0,9.0,3.0]],
     "D": [[1.0,2.0],[3.0,4.0]],
     "E": [[6.0,5.0,3.0,6.0],[9.0,1.0,6.0,7.0],[6.0,6.0,8.0,1.0],[2.0,4.0,4.0,4.0]]}

print(gauss_jordan(d['A']))
