#Produit par Guillaume JADIN
def multiply (A, B):
    """
    Multiplie deux matrices et/ou vecteurs A et B.
    ---------------------------------------------------
    pre: A et B sont des matrices ou des vecteurs
         Nombre de lignes de A = nombre de colonnes de B
    post: Retourne le resultat de la multiplication matricielle
          Retourne une TypeError : Dimension mismatch si le nombre de lignes A != au nombre de colonnes B
    """
    if type(A[0]) == float: #regle les problemes d'index avec un vecteur.
        A = [A]
    if type(B[0]) == float: #idem
        B = [B]
        
    m, p = len(B[0]), len(A) #etabli la longeur mxp de la nouvelle matrice
    
    if m != p: #leve une erreur si m != p
        raise TypeError("Dimension mismatch")
    
    f_matrix = [[0]*p for _ in range(m)] #cree la nouvelle matrice
    
    for lines_a in range(len(A)): #boucle sur les lignes de A
        for columns_b in range(len(B[0])): #boucle sur les colonnes de B
            for g in range(len(B)): #etabli la constante k (dans ce cas, g)
                f_matrix [lines_a] [columns_b] += A[lines_a] [g] * B[g] [columns_b] #appliquer la multiplication + ajouter a f_matrix
    
    return f_matrix 


d = {"A": [1.0,2.0],
     "B": [3.0,4.0],
     "C": [1.0,2.0,3.0],
     "D": [[1.0,2.0],[3.0,4.0]],
     "E": [[1.0,2.0,3.0],[4.0,5.0,6.0]]}

print(multiply(d["D"], d["A"]))
