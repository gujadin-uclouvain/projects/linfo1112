########################################################
#-----------Devoir 3 - Algèbre - 17 Mai 2019-----------#
# Rédigé par Guillaume Jadin & Hugo Vervier (SINFBAC1) #
########################################################

import numpy as np

def qrSchmidt(A):
    """
    Effectue la décomposition QR de la matrice A par la méthode de Gram-Schmidt.

    :pre: A est une instance du type numpy.array()
    :post: Renvoi une matrice Q et une matrice R
    """
    size = A.shape

    if size[0] < size[1]:  # True si la matrice A est plus large que haute
        raise Exception("FatMatrixException")
    if np.linalg.matrix_rank(A) != size[1]:  # True si la matrice A n'a pas un rang-colonne plein
        raise Exception("SingularMatrixException")

    def find_q(A):
        """
        Calcule la matrice Q

        :pre: A est une instance du type numpy.array()
        :post: Renvoi une matrice Q
        """
        Q = np.zeros((size[0], size[1]))  # Crée la matrice Q vide
        computing = None

        column_index = 0
        while column_index < size[1]:
            column = A[:, column_index]

            if column_index > 0:
                for nbr_q in range(0, column_index+1):
                    if nbr_q == 0:
                        computing = column
                    else:
                        q_column = Q[:, nbr_q-1]

                        computing = computing - ( ((np.dot(column, q_column)) / (np.dot(q_column, q_column))) * q_column)

            if column_index == 0:
                computing = column

            norm = computing / np.sqrt(np.dot(computing, computing))
            Q[:, column_index] = norm

            column_index += 1
        return Q

    def find_r(A, Q):
        """
        Calcule la matrice R

        :pre: A & Q sont des instances du type numpy.array()
        :pre: Q est une matrice trouvée grâce à la fonction find_q(A)
        :post: Renvoi une matrice R
        """
        R = np.zeros((size[1], size[1]))  # Crée la matrice R vide

        column_index = 0
        while column_index < size[1]:
            for line_index in range(0, column_index+1):
                R[line_index, column_index] = np.dot(A[:,column_index], Q[:,line_index])
            column_index += 1
        return R

    Q = find_q(A)
    R = find_r(A, Q)
    return Q, R
